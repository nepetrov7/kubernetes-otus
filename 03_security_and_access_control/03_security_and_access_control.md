# Безопасность и управление доступом

`kubectl api-resources`
    - можно посмотреть api group
    - в колонке namespaced - уходит ли оно в namespace
    - короткие имена ресурсов
## kubernetes можно подружить c LDAP, группы и имена пользователей имеют смысл только для механизмов авторизации
- в kubernetes API нет сущностей,связанных с конкретными пользователями - пользователей, групп, паролей
- виды пользователей:
    - обычные пользователи:
        - Это люди, которые отжают команды кластеру
        - Глобальны в рамках кластера
        - не управляются из API
    - `Service Accounts` - как только он создается - вместе с ним создается `Secret` и из коробки выдается токен, который привязывается к аккаунту
        - Привязаны к жихни ресурса или процесса в кластере
        - локальны в Namespace
        - Управляются из API
        - Привязаны к токену из Secrets, позволяют элементам кластера общаться с API
- варианты аутентификации:
    - X509 Client Certs - самый частый способ
        - просто добавить сертификат для пользователя
        - kubernetes  не поддерживает отзыв сертификата
        - можно сделать их короткоживущими, но это много телодвижений
    - Static Token File /Static Password File
        - не имеют срока жизни
        - придется перезагружать api-server, если нужно будет поменять что-то, например сам файл с токеном
    - Bootstrap Tokens /Service Account Tokens
    - OpenID Connect Tokens
        - внешняя система рулит аутентификацией
    - Webhook Token Authentication
    - Authenticating Proxy
    - Анонимный запрос - можно настроить что анонимный пользователь сможет видеть, по умолчанию - ничего
- kubectl -n kube-system describe po kube-apiserver
    - параметры kube-apiserver: `https://kubernetes.io/docs/reference/command-line-tools-reference/kube-apiserver/`
- настраиваем аутентификацию X509 Client Certs
    1. `openssl genrsa -out vanya.key 4096` - генерируем ключ
    1. создаем файл `csr.cnf` - в котром описываем для кого этот сертификат и к какой группе он относится
    1. `openssl req -config csr.cnf -new -key vanya.key -nodes -out vanya.csr` запрос на подпись сертификата
    1. `export BASE64_CSR=$(cat vanya.csr | base64 -wrap=0)` - помещаем в переменную зашифрованный запрос на подпись сертификата
    1. создаем манифест `csr.yaml` для добавления сертифката в кластер, в котром пишем переменную `BASE64_CSR`
    1. `cat csr.yaml | envsubst | kubectl apply -f -` передаем yaml манифест в утилиту envsubst, эта утилита находит в манифесте переменную и заменяет ее реальным значением переменной и передает ее дальше по конвейеру в kubectl
    1. `kubectl get csr` - проверить появился ли этот запрос
    1. `kubectl certificate approve vanya_csr` - подтвердить и подписать апрос
    1. `kubectl get csr vanya_csr -o jsonpath={.status.certificate} | base64 -d > vanya.crt` - расшифровываем подписанный сертификат и пишем в файл
    1. `openssl x509 -in vanya.crt --text` прочитать сертифкат в виде текста, просмотреть все параметры
    1. `kubectl cluster-info` - получить отсюда информацию о cluster-api, по какому адресу можно к нему обратиться
    1. `kubectl config --kubeconfig=./config set-cluster k8s --server=https://IP:PORT --certificate-authority=/directory_with_cluster_certs/ca.crt --embed-certs=true` собрать конфиг с использованием сертификата и адреса API сервера и имени кластера
    1. `kubectl config --kubeconfig=./config set-credentials vanya --client-key=vanya.key --client-certificate=vanya.crt --embed-certs=true` - добавляем в конфиг информауцию о пользоавтеле и его сертификате
    1. `kubectl config --kubeconfig=./config set-context default --cluster=k8s --user=vanya --namespace default` - связываем кластер с пользователем + неймспейс, в котором пользователь будет работать (можно не указывать)
    1. конфиг можно подложить в `~/kube/config` или использовать переопределение конфига каждый раз: `kubectl --kubeconfig=./config` либо поместить конфиг в переменную окружения `KUBECONFIGFILE=`
    1. `kubectl config --kubeconfig=./config use-context default` - задать контекст по умолчанию
- роли бывают:
    - Role - ограниченные нейспейсом, можно добавлять новые нейспейсы по мере добавления в кластер. RoleBinding - привязка к пользователю
    - ClusterRole - не ограниченные нейспейсом. ClusterRoleBinding - привязка к пользователю
        - `kubectl get clusterrole` - весь список
- настройка авторизации
    1. создаем Role или ClusterRole (связывание операций и ресурсов)
    ```
    apiVersion: rbac.authorization.k8s.io/v1
    kind: Role
    metadata:
        namespace: default
        name: pod-reader
    rules:
      - apiGroups: [""] # "" означает apiGroup под именем core или legacy
        resources: ["pods"]
        verbs: ["get", "watch", "list"]
    # apiGroup: rbac.$(вот тут вписываем api-group) api group берем в "kubectl api-resources"
    ```
    1. создаем RoleBinding или ClusterRoleBinding (привязку к пользователю) `kubectl get clusterrolebinding` - весь список
    ```
    apiVersion: rbac.authorization.k8s.io/v1
    kind: RoleBinding
    metadata:
        name: read-pods # под этим именем мы потом увидим этот RoleBinding
        namespace: default
    subjects:
      - kind: User # Group, ServiceAccount
        name: vanya # имя чувствительно к регистру
        apiGroup: rbac.authorization.k8s.io
    roleRef:
        kind: Role # явно указываем Role или ClusterRole
        name: pod-reader # а тут имя той Role или ClusterRole к которой мы биндимся
        apiGroup: rbac.authorization.k8s.io
    ```
    1. чтобы разрешить поду обращаться к api-server, нужно примаунченому ServiceAccount-у дать на это права:
    ```
    apiVersion: rbac.authorization.k8s.io/v1
    kind: RoleBinding
    metadata:
        name: sa-pods
        namespace: default
    subjects:
      - kind: ServiceAccount
        name: dafault
    roleRef:
        kind: Role
        name: pod-reader
        apiGroup: rbac.authorization.k8s.io
    ```
- наглядно как обращаются поды к API-server
    - kubernetes при создании namespace создает сервис аккаунт по умолчанию: `kubectl -n test get sa`
    - `kubectl run alpine -it --image=alpine /bin/sh` - создать под с alpine и запустить в нем терминал
    - `/var/run/secrets/kubernetes.io/serviceaccount` - здесь в контейнере есть ссылки на сертификат, на токен и на namespace
    - `curl --cacert $CA_CERT -H "Authorization: Bearer $ TOKEN" "https://kubernetes/api/v1/namespaces/$NAMESAPACE/servoces/"` - обратиться к API-server из контейнера

полезные плагины для kubectl: https://www.freshbrewed.science/k8s-and-krew-rbac-utilities/index.html  


## admission controllers
- Kubernetes предлагает еще одну функцию ограничения доступа
- AC может делать две важные функции:
    - Изменять запросы к API (JSON Patch)
    - Пропускать или отклонять запросы к API
- Каждый контроллер может делать обе вещи, если захочет
- Какие-то AC включены сразу же из коробки 
    - `kubectl cluster-info dump | grep enable-admission`
- NamespaceLifecycle
    - Запрещает создавать новые объекты в удаляемых Namespaces
    - Не допускает указания несуществующих Namespaces
    - Не дает удалить системные Namespaces
        - default
        - kube-system
        - kube-public
- ResourceQuota
    - Определяется для namespace
    - Ограничивает
        - кол-во объектов
        - общий объем ресурсов
        - объем дискового пространства для volumes
- LimitRanger
    - Определяется для namespace
    - Возможность принудительно установить ограничения по ресурсам pod-а
- NodeRestriction
    - Ограничивает возможности kubelet по редактированию Node и Pod
- ServiceAccount
    - Автоматически подсовывает в Pod необходимые секреты для функционирования Service Accounts
- Mutating + Validating AdmissionWebhook
    - Позволяют внешним обработчикам вмешиваться в обработку запросов, идущих через AC