# Сетевая подсистема kubernetes
### local DNS Cache - по дефолты ставится в kubespray, но модно отключить при установке. и не ставится kubeadm-ом, нужно ставить отдельно
- в /etc/kubernetes/nodelocaldns-config.yml конфиг nodelocaldns. либов в `kubectl -n kube-system get cm nodelocaldns -o yaml`
    - `data.Corefile.$name_cluster:53.bind` - установлен IP адрес nodelocaldns. когда под в кластере хочет выяснить адрес какого-то сервиса в кластере, то запрос идет в первую очередь сюда
    - по дефолту там 169.254.25.10 - он же в `/etc/resolv.conf` как днс вписан. это locallink (169.254.), не выйдет наружу куда-то.
    - `forward . 172.24.128.3` - ip coredns (`kubectl -n kube-system get svc coredns `) - если в nodelocaldns не найдется ответ, то обратиться в coredns
    - udp по nat может потеряться, поэтому используется force_tcp, принудительно tcp
- /etc/kubernetes/coredns-config.ylm - `kubectl -n kube-system get cm coredns -o yaml`

- Роль kubelet
    - Из файла `/var/lib/kubelet/config.yaml`
        - Адреса резолверов - параметр clusterDNS
        - Домен кластера - параметр clusterDomain

- dnsConfig
    - Мы в любой момент можем прописать дополнительные опции к `/etc/resolv.conf` в Pod при помощи секции `dnsConfig`
    - Она работает с любым `dnsPolicy` ,но становится обязательной,если мы выставляем `dnsPolicy: None`
- Внутри кластера возможно обращение по следующим именам
    - Обращение к сервису внутри namespace: `service`
    - Обращение к сервису внутри кластера: `service.namespace`
    - FQDN сервиса: `service.namespace.svc.<домен кластера>.`
    - FQDN пода: `10-0-0-1.ns.pod.cluster.local` (Deprecated, но пока работает)

### services
- виды сервисов для внутрикластерных взаимодействий
    - ClusterIP (по умолчанию)
        - `ClusterIP` удобны в тех случаях, когда:
            - Нам не надо подключаться к конкретному поду сервиса
            - Нас устраивается случайное расределение подключений между подами
            - Нам нужна стабильная точка подключения к сервису, независимая от подов, нод и DNS-имен
        - в `spec.selector:` - указываем лейблы, если в каком-то поде есть хотябы один из тех же `key:value` - это значит что этот сервис будет его обслуживать
        - в `spec.ports.targetPort:` - указываем порт, который слушает под
        - в `spec.ports.port` - указываем порт, на который мы будем обращаться, обращаясь к сервису
        - можно задавать несколько портов  (multi-port), например для http и https
        - `kubectl describe svc my-service` - если в Selector все правильно указали, то в Endpoints появятся IP подов, подходящих по лейблам
            - эти endpoints берутся из `kubectl get entpoints $nameService`
        - для проверки маршрутов и балансировки: `iptables-save | grep "my-service"`
    - NodePort
        - на каждой ноде открывается один и тот же порт из особого диапазона
            - за эот отвечает параметр `kube-apiserver` под именем `--service-node-port-range`
            - по умолчанию это 30000-32767 (лучше не менять)
            - можно прописать в `spec.ports.nodePort:` - порт, в котором сервис будет принимать трафик
        - каждый делает dnat на адреса Pods
    - LoadBalancer
        - этот вид сервиса предназанчен только для облачных провайдеров, который своими программными средствами реализуют балансировку нагрузки
        - в bare metal это делается чуть иначе - понадобится MetalLB в kubespray этот функционал есть
        - `kubectl get svc` тут у сервиса в столбце EXTERNAL-IP - будет выдан IP

## ingress
- Объект управляющий внешним доступом к сервисам внутри кластера. Обеспечивает:
    - Организацию единой точки входа в приложения снаружи
    - Балансировку трафика
    - Терминацию SSL
    - Виртуальный хостинг на основе имен и т.д.
    - Работает на L7 уровне.

### Ingress Controller
- Ingress - набор правил внутри кластера Kubernetes. Для применения данных правил нужен Ingress который состоит из 2-х функциональных частей:
    - Приложение, которое отслеживает через Kubernetes API новые объекты Ingress и обновляет конфигурацию балансировщика
    - Сам балансировщик (nginx, HAProxy, traefik, …),который отвечает за управление сетевым трафиком
    - https://kubedex.com/ingress/ - виды ingress Controller и их возможности
    - https://kubernetes.github.io/ingress-nginx/deploy/baremetal/ - обзор про затаскивание внешнего трафика
    - hostNetwork: true - тогда под видит напрямую все сетевые адаптеры ноды

### команды
- kubectl delete pod foo --grace-period=0 --force - удалить принудительно за 0 секуунд. 0 секунд только с --force. от 1 секунды можно без --force
- kubectl delete pod foo --now - удалить с минимальной задержкой
