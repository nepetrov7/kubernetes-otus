## Контроллеры  
это процесс, который смотрит состояние чего-либо  
например датчики (анализатор данных датчиков - это простой контроллер) - контроллер не только может собирать,но и анализировать и вносить изменения в среду
- Observe - наблюдение за актуальным состоянием кластера
- Analyze - определение различий с желаемым состоянием
- Act - выполнение набора действий для достижения желаемого состояния

- `kubectl -n kube-system logs -f -l component=kube-controller-manager` tail -f (аналог) можно в kube-controller-manager посмотреть логи создания подов
- все контроллеры по запуску подов находятся в kube-controller-manager (replicaset-controller, daemonset-controller и тд) они смотрят в kube-apiserver и берут инфу о необходимом состоянии а затем выполняют необходимые действия для того чтобы привести кластер в соотвествующее состояние (если это ссделать должны они)
- все это работает по протоколу http, данныеы передаются друг другу в формате json, используется механизм "Long Polling"(не закрывающееся tcp соединение) и отчасти инфа кешируется
- в customResource можно помещать информацию, конфиги для контроллеров
- в customResourceDefinition описываем метаинформацию о customResource (в том числе короткое имя и т.д.)
    - еще опиcывается схема openAPIV3Schema
- есть operator framework  для написания операторов
