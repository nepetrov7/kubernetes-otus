base=http://127.0.0.1:8001
ns=namespaces/default

curl -N -s $base/api/v1/$ns/pods?watch=true | jq . | grep -E 'ADDED|MODIFIED|DELETED'