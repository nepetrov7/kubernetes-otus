
## volume - абстракция реального хранилища
- создается и адаляется с подом
- основные типы volumes(томов): 
    - emptyDir - либо каталог на ноде, либо в оперативной памяти (tmpfs). с этой областью могут работать все контейнеры пода. как кейс - initContainer подгатавливает директорию и далее уже в предподготовленной директории запускается основной контейнер.
    - hostPath - конкретный путь до директории ноды, например fluentd берет данные из /var/log, обрабатывает и шлет их в elastic
    - nfs
    - CephFS и ClusterFS
    - FC и iSCSI
    - прочие volumes
        - configMap содержит конфигурацию приложений и/или значения переменных окружения отдельно от конфигурации пода
        - Secret - зашифрованные переменные
- особенности работы:
    - configMap или secret нужно описывать в отдельном манифесте
    - затем в манифесте пода вписать его имя в volumes
    - в описании контейнера указать этот volume и куда его примонтировать в разделе volumeMounts
    - если мы монтируем volume через subPath, то обновления в контейнере не будет

## persistent volumes
- создаются на уровне кластера
- имеют отдельный от сервисов жизненный цикл
- persistenVolumeClaim - запрос на хранилище от пользователя
    - у них указывается права доступа и объем
1. админ кластера утснавливает некий тип сетевоого хранилища
1. админ создает том PV, отправляя дескриптор pv в API kubernetes
1. пользователь создает заявку PersistentVolumeClaim
1. kubernetes находит PV подходящего размера и режима доступа и связывает заявку PVC с томом PV
1. пользователь создает подууль с томом, ссылающимся на заявку PVC
- политики переиспользования PV
    - retain - после удаления pvc, pv переходит в состояние "released" админ имеет возможность сохранить данные, удалить pv и освободить место во внешнем хранилище
    - delete - удаляет pv вместе с pvc
    - recycle(deprecated) - удаляет все содержимое pv и делает его доступным для использования

### лимитирование ресурсов
limitRange - администратор кластера может ограничить:
- количество PVC в неймспейсе
- размер хранилища, который может запросить PVC
- объем хранилища, который может иметь нейспейс

## storageClasses
- можно использовать динамический provisionning
- сам создает pv
1. админ определяет поставщика PV
1. создает storageClass
1. пользователь создает заявку PVC, ссылаясь на SC
1. kubernetes просит поставщика зарезервировать новый ресурс PV, основываясь на запрошенном режиме хранилища в заявке PVC и параметрах в классе SC
1. поставщик резервирует фактическое хранилище, создает ресурс PV и связывает его с заявкой PVC
1. пользователь создает модуль с томом, ссылающимся на заявку PVC по имени

## statefulSet  
отличается от deployments тем что:
- каждый под имеет уникальное состояние (имя, сетевой адрес, volumes)
- для каждого создается отдельный PVC (volumeClaimTemplates)
- при удалении подов, тома, связанные с ними - не удаляются
