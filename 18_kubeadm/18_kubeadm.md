подготовка ммашин:
    - для etcd используется протокол записи - пока не будет записано на все ноды, не считается записаным. поэтому чтобы все не тормозило - нужны быстрые диски и запрет на размещение на эти ноды подов.
    - на нодах всегда надо отключать swap, kubernetes не умеет работать со swap, поэтому контейнеры могут тормозить.
    - нужно включать максимальную производительность CPU ( scaling_governor ) чтобы ОС не пыталась экономить процессорное время
    - Включить маршрутизацию ( ip_forward )
установка:
    - kubeadm
        - Позволяет настраивать master и worker ноды, подключать их к кластеру
        - Не деплоит ничего дополнительно (f.e. CNI)
        - HA режим
        - Позволяет обновить версию кластера
        - [создание кластера с помощью kubeadm](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/create-cluster-kubeadm/)
    - kubespray
        - Ansible playbook для установки/обновления Kubernetes
        - Поддерживает большинство популярных Linux-дистрибутивов
        - Выбор множества сетевых плагинов
        - HA режим
        - Можно подкрутить под себя, Ansible знают все
        - Нужно следить, чтобы периодически прогонялся сценарий целиком
        - [ссылка на github](https://github.com/kubernetes-sigs/kubespray)
    - kops
        - Развертывает Kuberntetes в облачных провайдерах (AWS, GCE, Digital Ocean)
        - Развертывает HA master-ноды
        - Конфигурация описывается в манифестах
        - Есть отставание по поддерживаемым версиям kubernetes (сейчас отстает на 1 версию)
        - [ссылка на githud](https://github.com/kubernetes/kops)
    - Rancher
        - С помощью Terraform создает все ноды для кластера
        - Может устанавливать уже на существующих машинах
        - Поддерживает HA режим
        - Графический интерфейс
        - [rancher.com](https://rancher.com/)
        - из минусов - специфичный, делает многое на свой лад
- Реакция на ошибки
    - kubelet обновляет статус каждые `--node-status-update-frequency` (по умолчанию 10s )
    - kube-controller-manager проверяет статус kubelet каждые `--nodemonitor-period` секунд (по умолчанию 5s )
    - Если статус не меняется за `--node-monitor-grace-period` секунду (по умолчанию 40s ), kube-controller-manager пересмотрит статус kubelet
    - `--pod-eviction-timeout` таймаут после которого поды будут убраны с ноды в случае ее failed/unknown статуса (по умолчанию 5 m )
- LimitRange - можно установить максимальные лимиты для подов в неймпейсе. можно задать дефолтные лимиты для подов, на случай, если ничего не указали.
```
apiVersion: v1
kind: LimitRange
metadata:
  name: cpu-resource-constraint
spec:
  limits:
  - default: # this section defines default limits
      cpu: 500m
    defaultRequest: # this section defines default requests
      cpu: 500m
    max: # max and min define the limit range
      cpu: "1"
    min:
      cpu: 100m
```
- можно выставить квоту на ресурсы в одном неймпейсе
```
apiVersion: v1
kind: ResourceQuota
metadata:
  name: compute-resources
spec:
  hard:
    requests.cpu: "1"
    requests.memory: 1Gi
    limits.cpu: "2"
    limits.memory: 2Gi
    requests.nvidia.com/gpu: 4
```
- PodDisruptionBudget позволит держать всегда нужное количество реплик, даже при эвакуации подов
```
apiVersion: policy/v1
kind: PodDisruptionBudget
metadata:
  name: zk-pdb
spec:
  minAvailable: 2
  selector:
    matchLabels:
      app: zookeeper
```
- PriorityClass - позволяет управлять приоритетом подов, если по ресурсам чего-то будет не хватать, то он убьет под с более низким приоритетом и развернет с более высоким
```
---
apiVersion: v1
kind: Pod
metadata:
  name: nginx
  labels:
    env: test
spec:
  containers:
  - name: nginx
    image: nginx
    imagePullPolicy: IfNotPresent
  priorityClassName: high-priority
---
apiVersion: scheduling.k8s.io/v1
kind: PriorityClass
metadata:
  name: high-priority-nonpreempting
value: 1000000
preemptionPolicy: Never
globalDefault: false
description: "This priority class will not cause other pods to be preempted."
```
- Автомасштабирование кластера | Cluster autoscaler
    - Cluster autoscaler меняет размер кластера когда:
        - Некуда назначить под, т к нет достаточного кол-ва ресурсов
        - Есть низкозагруженные ноды, нагрузку с которых можно перенести на другие ноды
        - Можно использовать с HPA
        - Хорошо дополняется pod priorities
        - Иногда нужно приделывать overprovisioning
        - в [репозитории](https://github.com/kubernetes/autoscaler/tree/master/cluster-autoscaler) есть список поддерживаемых провайдеров
    - На масштабирование уходит не больше 30 секунд в небольших кластерах (меньше 100 нод и до 30 подов на каждой), средняя задержка 5 секунд
    - На масштабирование уходит не больше 60 секунд в больших кластерах (100-1000 нод), средняя задержка 15 секунд
    - [сценарии тестирования](https://github.com/kubernetes/autoscaler/blob/master/cluster-autoscaler/proposals/scalability_tests.md)
    - 